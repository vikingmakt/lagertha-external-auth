var auth         = require('lagertha').auth;
var ee           = require('lagertha').eventemitter;
var localStorage = require('lagertha').storage.local;
var navigate     = require('lagertha').http.navigate;
var options      = require('lagertha').options;
var query        = require('lagertha').http.query;


var STATUS = Object.freeze({
  UNKNOW: '80598c860d2141b5a7aff55f12f27035',
  WAITING: '17faffa512af42079d301c9a8e8cdd76',
  LOGGED: '835adc541d2c4fd5bacc4ec4989353b6'
});

var sessionCode   = localStorage(options.auth.sessionCode);
var sessionStatus = localStorage(options.auth.sessionStatus);

ee.on(auth.events.AUTH.ERROR, function onAuthError() {
  sessionCode.remove();
  sessionStatus.remove();
  verify();
});

ee.on(auth.events.AUTH.LOGOUT, function onLogout() {
  sessionCode.remove();
  sessionStatus.remove();
});

function waitingSession() {
  return sessionStatus.get() == STATUS.WAITING;
}

function hasSession() {
  return sessionCode.has();
}

function save(code) {
  sessionCode.set(code);
  sessionStatus.set(STATUS.LOGGED);
}

function verify() {
  if(waitingSession()) {
    return completeSign();
  }

  if(!hasSession()) {
    return requestSign();
  }

  return sessionSign();
}

function completeSign() {
  var Query = query.get();

  if(!Query.session) {
    sessionStatus.set(STATUS.UNKNOW);
    return verify();
  }

  if (Query.session) {
    save(Query.session);
  }

  if (Query.path) {
    navigate(Query.path);
  }

  return true;
}

function requestSign() {
  sessionStatus.set(STATUS.WAITING);
  window.location.assign([options.sign.url,
                          '?app_url=' + window.location.href,
                          '&path=' + window.location.pathname
                         ].join(''));
}

function sessionSign() {
  if (hasSession()) {
    auth.bySession(sessionCode.get());
  }
}

module.exports = {
  logout: auth.logout,
  verify: verify
};
